#!/usr/bin/env python

import os
import json
import tempfile
import subprocess

tasks = json.loads(subprocess.check_output(['task', 'status:pending', 'export']))

sorted_tasks = sorted(tasks, key=lambda x: x['urgency'], reverse=True)

task_string = []

for t in sorted_tasks:
    id = str(t['id']).zfill(3)

    project = '{}'.format(t.get('project', 'none').strip())
    description = t['description'].strip()

    s = f"<b>{round(float(t['urgency']))}</b> <b>[{id}]</b> - <b>{project}</b>\n{description}"
    task_string.append(s)

wofi = subprocess.Popen(['wofi', '-m',  '-d', '-p', 'Select Task to Describe', '-k', '/dev/null', '-Ddmenu-separator=|'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out = wofi.communicate('|'.join(task_string).encode())[0].decode()

if len(out) > 0:
    tid = int(out[4:7])

    fd, filename = tempfile.mkstemp()

    try:
        info = subprocess.check_output(['task', str(tid), 'info'])

        with os.fdopen(fd, 'w') as f:
            f.write(info.decode())

        subprocess.check_output(['alacritty', 'vim', filename])
    except Exception as e:
        print(str(e), flush=True)
    finally:
        os.remove(filename)
