#!/usr/bin/env python

import json
import subprocess

tasks = json.loads(subprocess.check_output(['task', 'status:pending', 'export']))

sorted_tasks = sorted(tasks, key=lambda x: x['id'])

task_string = []

for t in sorted_tasks:
    id = str(t['id']).zfill(3)

    project = '{}'.format(t.get('project', 'none').strip())
    description = t['description'].strip()

    s = '<b>[{}]</b> - <b>{}</b>\n{}'.format(id, project, description)
    task_string.append(s)

wofi = subprocess.Popen(['wofi', '-m', '-d', '-p', 'Select Task to Delete', '-k', '/dev/null', '-Ddmenu-separator=|'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out = wofi.communicate('|'.join(task_string).encode())[0].decode()

if len(out) > 0:
    tid = int(out[4:7])

    wofi = subprocess.Popen(['wofi', '-m', '-d', '-p', 'Are you sure you want to remove task {}?'.format(tid), '-k', '/dev/null'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = wofi.communicate('No\nYes'.encode())[0].decode().strip()

    if out == 'Yes':

        rm = subprocess.Popen(['task', 'rm', str(tid)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = rm.communicate(input='yes\n'.encode())

        print(out[0].decode())
