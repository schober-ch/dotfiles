#!/usr/bin/env python

import os
import json
import tempfile
import subprocess

tasks = json.loads(subprocess.check_output(['task', 'status:pending', 'export']))

sorted_tasks = sorted(tasks, key=lambda x: x['id'])

task_string = []

for t in sorted_tasks:
    id = str(t['id']).zfill(3)

    project = '{}'.format(t.get('project', 'none').strip())
    description = t['description'].strip()

    s = '<b>[{}]</b> - <b>{}</b>\n{}'.format(id, project, description)
    task_string.append(s)

wofi = subprocess.Popen(['wofi', '-m', '-d', '-p', 'Select Task to Annotate', '-k', '/dev/null', '-Ddmenu-separator=|'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out = wofi.communicate('|'.join(task_string).encode())[0].decode()

if len(out) > 0:
    tid = int(out[4:7])

    fd, filename = tempfile.mkstemp()

    try:
        subprocess.check_output(['kitty', 'vim', filename])

        with os.fdopen(fd, 'r') as f:
            data = f.read()
            if len(data) > 0:
                print(subprocess.check_output(['task', 'annotate', str(tid), data]))
    except Exception as e:
        print(str(e), flush=True)
    finally:
        os.remove(filename)

