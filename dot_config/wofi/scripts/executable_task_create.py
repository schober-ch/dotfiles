#!/usr/bin/env python

import sys
import json
import subprocess

paste = False

if len(sys.argv) == 2 and sys.argv[1] == 'clipboard':
    paste = True

tasks = json.loads(subprocess.check_output(['task', 'export']))

projects = {x['project'] for x in tasks if 'project' in x}
projects = sorted(list(projects))

projects.insert(0, 'none')

wofi = subprocess.Popen(['wofi', '-m', '-d', '-p', 'Select Project', '-k', '/dev/null'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
proj = wofi.communicate('\n'.join(projects).encode())[0].decode()
proj = proj.strip()

if len(proj) > 0:
    content = None
    if paste:
        try:
            content = subprocess.check_output(['wl-paste', '-n', '-p']).decode()
        except:
            pass

    if content:
        desc = subprocess.check_output(['wofi', '-d', '--lines', '1', '-p', 'Task Description', '--search', content])
    else:
        desc = subprocess.check_output(['wofi', '-d', '--lines', '1', '-p', 'Task Description'])

    desc = desc.strip()

    if len(desc) > 0:
        print(subprocess.check_output(['task', 'add', 'project:{}'.format(proj), desc]))


