#!/bin/bash
HOST=194.95.98.38
USERNAME=cschober
PASSWORD=$(keepassxc-cli show -sa password /home/christoph/Nextcloud/thd_passwords.kdbx "THD Nutzeraccount")

if nc -w 2 -z ${HOST} 445 2>/dev/null; then
  sudo mount -t cifs -o user=${USERNAME},password=${PASSWORD},uid=1000,gid=1000 //${HOST}/verteiler /mnt/verteiler
  sudo mount -t cifs -o user=${USERNAME},password=${PASSWORD},uid=1000,gid=1000 //${HOST}/cschober$ /mnt/home
else
  sudo umount -lf /mnt/verteiler
  sudo umount -lf /mnt/home
fi
