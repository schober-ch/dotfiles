#!/bin/bash

if swaymsg -t get_tree | jq -e '.nodes[].nodes[].floating_nodes[] | select(.app_id=="org.keepassxc.KeePassXC") | select(.visible==true)' > /dev/null
then
  swaymsg "[app_id=org.keepassxc.KeePassXC] move scratchpad"
else
  swaymsg "[app_id=org.keepassxc.KeePassXC] focus"
fi

