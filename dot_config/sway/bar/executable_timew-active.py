#!/usr/bin/env python3

import subprocess
import datetime
import dateutil.parser
import json

try:
    a = subprocess.check_output(["timew", "get", "dom.active.json"])
    b = json.loads(a)
except subprocess.CalledProcessError:
    b = {'start': None, 'tags': ['inactive']}

elapsed = ''
if not 'inactive' in b['tags']:
    now = datetime.datetime.now().astimezone()
    start = dateutil.parser.parse(b['start']).astimezone()
    elapsed = (now - start)
    m, s = divmod(elapsed.total_seconds(), 60)
    h, m = divmod(m, 60)
    if h == 0:
        elapsed = "{}min".format(int(m))
    else:
        elapsed = "{0}:{1}h".format(int(h),int(m))

print("tw: {0} {1}".format(" ".join(b['tags']), elapsed))
