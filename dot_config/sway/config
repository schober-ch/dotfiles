# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu dmenu_path | wofi --show drun -I | xargs swaymsg exec --

font pango:DejaVu Sans Mono 11

# set names for workspaces
#set $ws1 1: www
#set $ws2 2
#set $ws3 3
#set $ws4 4
#set $ws5 5
#set $ws6 6
#set $ws7 7
#set $ws8 8
#set $ws9 9 
#set $ws10 10
set $wsMail ✉

workspace_auto_back_and_forth yes

# autostart
exec --no-startup-id mako
exec keepassxc

exec nm-applet --indicator

bindsym $mod+t exec thunar
# special focus
bindsym $mod+m [app_id="thunderbird"] focus
bindsym $mod+z [app_id="Zotero"] focus
bindsym $mod+h exec copyq toggle

# Notifications
bindsym Control+Space exec makoctl dismiss
bindsym Control+Shift+Space exec makoctl dismiss --all

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)

set $cmon "Philips Consumer Electronics Company PHL 279P1 UK82331004635"
set $rmon "Philips Consumer Electronics Company PHL 279P1 UK82331004632"

set $philips_res "3840x2160@59.997Hz"
set $rmon_pos_10 "3840 0"
set $rmon_pos_15 "2560 0"
set $rmon_pos_20 "1920 0"


set $tp_screen eDP-1
set $tp_res "1920x1200"

bindsym XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%

bindsym XF86AudioNext exec cmus-remote -n
bindsym XF86AudioPrev exec cmus-remote -r
bindsym XF86AudioPause exec cmus-remote -u
bindsym XF86AudioPlay exec cmus-remote -u

set $mode_display AUX SCREEN: (s) STANDALONE (d) DESK (r) DESK ROT (l) LECTURE 
mode "$mode_display" {
    bindsym s output $tp_screen enable, output $tp_screen pos 0 0 res $tp_res, output $cmon disable, output $rmon disable,  mode "default"
    bindsym d output $tp_screen disable,  output $cmon enable, output $cmon scale 1.4, output $cmon pos 0 0 res $philips_res, output $rmon enable, output $rmon scale 1.4, output $rmon pos 2560 0 res $philips_res, mode "default"
    bindsym r output $tp_screen disable,  output $cmon enable, output $cmon scale 1.4, output $cmon pos 0 600 res $philips_res, output $rmon enable, output $rmon scale 1.4, output $rmon transform 90, output $rmon pos 2743 0 res $philips_res, mode "default"
    #bindsym d output $tp_screen disable,  output $cmon enable, output $cmon scale 2, output $cmon pos 0 420 res $philips_res, output $rmon enable, output $rmon scale 2, output $rmon transform 90, output $rmon pos 1920 0 res $philips_res, mode "default"
    bindsym l output $tp_screen enable, output $tp_screen pos 0 0 res $tp_res, output $cmon disable, output $rmon disable, output HDMI-A-1 enable, output HDMI-1 pos 1920 0,  mode "default"

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+F5 mode "$mode_display"


#set $laptop eDP-1
#bindswitch --reload --locked lid:on output $laptop disable
#bindswitch --reload --locked lid:off output $laptop enable

#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
 #exec swayidle -w \
          #timeout 300 'swaylock -f -c 000000' \
          #timeout 600 'swaymsg "output * dpms off"' \
          #resume 'swaymsg "output * dpms on"' \
          #before-sleep 'swaylock -f -c 000000'
##
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
input * {
    xkb_layout "de"
    xkb_variant "deadgraveacute"
}
#
# Example configuration:
#
   input "2:7:SynPS/2_Synaptics_TouchPad" {
       dwt enabled
       tap enabled
       natural_scroll enabled
       middle_emulation enabled
   }
input "1267:12693:ELAN0676:00_04F3:3195_Touchpad" {
       dwt enabled
       tap enabled
       natural_scroll enabled
       middle_emulation enabled
}
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # kill focused window
    bindsym $mod+x kill

    # Start your launcher
    bindsym $mod+d exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

    # Exit sway (logs you out of your Wayland session)
#    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    #bindsym $mod+$left focus left
    #bindsym $mod+$down focus down
    #bindsym $mod+$up focus up
    #bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    #bindsym $mod+Shift+$left move left
    #bindsym $mod+Shift+$down move down
    #bindsym $mod+Shift+$up move up
    #bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#

# switch to workspace
# bindsym $mod+1 workspace $ws1
# bindsym $mod+2 workspace $ws2
# bindsym $mod+3 workspace $ws3
# bindsym $mod+4 workspace $ws4
# bindsym $mod+5 workspace $ws5
# bindsym $mod+6 workspace $ws6
# bindsym $mod+7 workspace $ws7
# bindsym $mod+8 workspace $ws8
# bindsym $mod+9 workspace $ws9
# bindsym $mod+0 workspace $ws10

#bindsym --no-repeat $mod+1 workspace $ws1; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+2 workspace $ws2; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+3 workspace $ws3; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+4 workspace $ws4; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+5 workspace $ws5; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+6 workspace $ws6; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+7 workspace $ws7; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+8 workspace $ws8; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+9 workspace $ws9; exec "echo 1 > /tmp/sovpipe"
#bindsym --no-repeat $mod+0 workspace $ws10; exec "echo 1 > /tmp/sovpipe"

#bindsym --release $mod+1 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+2 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+3 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+4 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+5 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+6 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+7 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+8 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+9 exec "echo 0 > /tmp/sovpipe"
#bindsym --release $mod+0 exec "echo 0 > /tmp/sovpipe"

## switch to workspace by numpad
#bindcode $mod+87 workspace $ws1
#bindcode $mod+88 workspace $ws2
#bindcode $mod+89 workspace $ws3
#bindcode $mod+83 workspace $ws4
#bindcode $mod+84 workspace $ws5
#bindcode $mod+85 workspace $ws6
#bindcode $mod+79 workspace $ws7
#bindcode $mod+80 workspace $ws8
#bindcode $mod+81 workspace $ws9
#bindcode $mod+90 workspace $ws10
##90, #87-#89, #83-#85, #79-#81

## move focused container to workspace
#bindsym $mod+Shift+1 move container to workspace $ws1
#bindsym $mod+Shift+2 move container to workspace $ws2
#bindsym $mod+Shift+3 move container to workspace $ws3
#bindsym $mod+Shift+4 move container to workspace $ws4
#bindsym $mod+Shift+5 move container to workspace $ws5
#bindsym $mod+Shift+6 move container to workspace $ws6
#bindsym $mod+Shift+7 move container to workspace $ws7
#bindsym $mod+Shift+8 move container to workspace $ws8
#bindsym $mod+Shift+9 move container to workspace $ws9
#bindsym $mod+Shift+0 move container to workspace $ws10

##move-to via numpad
#bindcode $mod+Shift+87 move container to workspace $ws1
#bindcode $mod+Shift+88 move container to workspace $ws2
#bindcode $mod+Shift+89 move container to workspace $ws3
#bindcode $mod+Shift+83 move container to workspace $ws4
#bindcode $mod+Shift+84 move container to workspace $ws5
#bindcode $mod+Shift+85 move container to workspace $ws6
#bindcode $mod+Shift+79 move container to workspace $ws7
#bindcode $mod+Shift+80 move container to workspace $ws8
#bindcode $mod+Shift+81 move container to workspace $ws9
#bindcode $mod+Shift+90 move container to workspace $ws10

# move workspace between outputs
bindsym $mod+Control+Shift+Right move workspace to output right
bindsym $mod+Control+Shift+Left move workspace to output left
bindsym $mod+Control+Shift+Down move workspace to output down
bindsym $mod+Control+Shift+Up move workspace to output up


bindsym $mod+q exec firefox
bindsym $mod+Escape exec ~/.config/sway/scripts/keepassxc.sh
bindsym $mod+l exec swaylock -f -c 000000

    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    #bindsym $left resize shrink width 10px
    #bindsym $down resize grow height 10px
    #bindsym $up resize shrink height 10px
    #bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Shift+Left resize shrink width 200px
    bindsym Shift+Down resize grow height 200px
    bindsym Shift+Up resize shrink height 200px
    bindsym Shift+Right resize grow width 200px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"


set $mode_system System: (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (S) shutdown, (R) UEFI
mode "$mode_system" {
    bindsym l exec $lock, mode "default"
    bindsym e exit
    bindsym s exec --no-startup-id systemctl suspend, mode "default"
    bindsym h exec --no-startup-id systemctl hibernate, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
    bindsym Shift+r exec --no-startup-id systemctl reboot --firmware-setup, mode "default"

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+e mode "$mode_system"

set $mode_grimshot Screen: (a) active, (o) output, (s) select, (w) window
mode "$mode_grimshot" {
    bindsym a exec grimshot --notify copy active; mode "default"
    bindsym o exec grimshot --notify copy output; mode "default"
    bindsym s exec grimshot --notify copy area; mode "default"
    bindsym w exec grimshot --notify copy window; mode "default"
    
    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym Print mode "$mode_grimshot"

bindsym $mod+Caret exec ct; mode "default"

# Task
set $task_create exec ~/.config/wofi/scripts/task_create.py
set $task_create_from_clipboard exec ~/.config/wofi/scripts/task_create.py clipboard
set $task_done exec ~/.config/wofi/scripts/task_done.py
set $task_annotate exec ~/.config/wofi/scripts/task_annotate.py
set $task_info exec ~/.config/wofi/scripts/task_info.py
set $task_remove exec ~/.config/wofi/scripts/task_remove.py
set $task_sync exec notify-send "Task Sync" "\n$(task sync 2>&1)"

#set $pomodoro_toggle exec ~/.config/waybar/modules/modules/pomodoro/pomodoro.sh toggle
#set $pomodoro_end exec ~/.config/waybar/modules/modules/pomodoro/pomodoro.sh end

## task
set $task Task (1) info (2) create (3) create from clip. (4) done (5) annotate (r) remove (s) sync
mode "$task" {
  bindsym --to-code --no-repeat {
    2 $task_create; mode "default"
    3 $task_create_from_clipboard;  mode "default"
    4 $task_done; mode "default"
    s $task_sync; mode "default"
    5 $task_annotate; mode "default"
    1 $task_info;  mode "default"
    r $task_remove;  mode "default"

    #p $pomodoro_toggle; $dismiss; $default
    #b $pomodoro_end; $dismiss; $default

    # back to normal: Enter or Escape
    Return mode "default"
    Escape mode "default"
  }
}
bindsym $mod+tab mode "$task"


#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
      font pango:DejaVu Sans Mono, Icons 12, Font Awesome 6 Free,
      position bottom
      status_command /usr/bin/i3status-rs ~/.config/sway/bar/config.toml
#      status_command waybar
      colors {
            separator #666666
            background #222222
            statusline #dddddd
            focused_workspace #0088CC #0088CC #ffffff
            active_workspace #333333 #333333 #ffffff
            inactive_workspace #333333 #333333 #888888
            urgent_workspace #2f343a #900000 #ffffff
      }
}

# FLOATING and specific window configs


for_window [app_id="com.cisco.anyconnect.gui"] floating enable
for_window [app_id="qalculate-gtk"] floating enable
for_window [app_id="com.github.hluk.copyq"] floating enable
for_window [instance="Msgcompose"] floating enable
for_window [app_id="thunderbird" title="Write: .*"] floating enable

# Special window assignments
assign [app_id="thunderbird"] workspace $wsMail
workspace $wsMail output $tp_screen

#exec zotero
exec thunderbird
exec rm -f /tmp/sovpipe && mkfifo /tmp/sovpipe && tail -f /tmp/sovpipe | sov -t 500
#exec swaylock -f -c 000000
exec /opt/work/sunpaper/sunpaper.sh -d
exec --no-startup-id python ~/.config/sway/scripts/inactive-windows-transparency.py
#exec wl-paste --watch cliphist store
exec copyq

exec --no-startup-id kanshi 

default_border none
#default_floating_border none
#smart_borders on
#smart_gaps on
#font pango:monospace 0
#titlebar_padding 1
#titlebar_border_thickness 0
include /etc/sway/config.d/*
include ~/.config/sway/config.d/*

