#!/bin/bash

TENS="1 2 3 4 5 6 7 8 9"

for i in $TENS; do
  sed "s/XX_TEN_XX/$i/g" workspaces.conf.template >>workspaces.conf
done
