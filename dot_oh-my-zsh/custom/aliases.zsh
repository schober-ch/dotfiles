alias cdc="cd ~/Documents/Courses"
alias cdl="cd ~/Documents/Lectures"

alias chezmoid="chezmoi"
alias lsync="git add . && git commit -m 'latest' && git push origin HEAD"
alias cat="bat --style=plain"
alias vim="nvim"

alias simian="java -jar /opt/simian-4.0.0/simian-4.0.0.jar"

alias si="swayimg"

alias ppwd="pwd | wl-copy"

alias ccd="cd `wl-paste`"

# alias ct="swaymsg 'exec alacritty --working-directory `pwd`'"

alias myip="curl https://ip.hetzner.com"
alias myip6="curl -6 https://ip.hetzner.com"
# eza aliases
#alias ld="eza -lD"
#alias lf="eza -lF --color=always | grep -v /"
#alias lh="eza -dl .* --group-directories-first"
#alias ll="eza -al --group-directories-first"
#alias ls="eza -lg --header --group-directories-first"
#alias lt="eza -al --sort=modified"
